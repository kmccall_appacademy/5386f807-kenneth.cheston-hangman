class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def play
    puts "Welcome to Hangman. Referee, please choose a word."
    setup
    take_turn until game_over?
    puts "Guesser has won!"
  end


  def setup
    secret_word = referee.pick_secret_word
    guesser.register_secret_length(secret_word)
    @board = Array.new(secret_word)
  end

  def take_turn
    guess = guesser.guess(board)
    indices = referee.check_guess(guess)
    update_board(guess, indices)
    guesser.handle_response(guess, indices)
  end

  def game_over?
    return false if board.any? { |el| el.nil? }
    return true
  end

  def update_board(guess, indices)
    if !(indices.empty?)
      indices.each { |index| board[index]=guess }
    end
  end
end

class HumanPlayer

  def register_secret_length(length)
    puts "Secret word is #{length} letters long."
  end

  def guess(board)
    print "#{board}\n"
    puts "Guess a letter!"
    gets.chomp
  end

  def pick_secret_word
    gets.chomp.length
  end

  def check_guess(guess)
    puts "Enter in indices where #{guess} occurs in word or simply enter a space to indicate zero indices (i.e. 3 or 5 3)"
    indices = gets.chomp
    if !(indices.empty?)
      indices.split(" ").map { |el| el.to_i }
    else []
    end
  end
end

class ComputerPlayer

  attr_reader :dict, :word, :candidate_words

  PATH = '/home/kenneth/5386f807-kenneth.cheston-hangman/lib/dictionary.txt'

  def initialize(dict = File.readlines(PATH).map(&:chomp))
    @dict = dict
    count = dict.size - 1
    @word = dict.shuffle[count]
    @candidate_words = dict
  end

  def handle_response(letter, indices)
    @candidate_words = @candidate_words.select do |word|
      letters = word.split("")
      (letters.each_index.select { |i| letters[i] == letter }) == indices
    end
  end


  def register_secret_length(length)
    @candidate_words = dict.select { |word| word.length == length }
  end

  def guess(board)
    print "#{board}\n"

    counter = Hash.new(0)
    arr = @candidate_words.join.split("")
    arr = arr.reject { |letter| board.include?(letter) }
    arr.each { |letter| counter[letter] += 1 }
    max = counter.values.max
    counter.select { |k, v| v == max }.to_a[0][0]
  end

  def pick_secret_word
    word.length
  end

  def check_guess(letter)
    indices = []
    (word.split("")).each_with_index {|el, idx| indices << idx if el == letter}
    indices
  end
end

if __FILE__ == $PROGRAM_NAME
  newgame = Hangman.new({referee: HumanPlayer.new, guesser: ComputerPlayer.new})
  newgame.play
end
